package com.demo.springboot.rest;

//import com.demo.springboot.service.SearchUnpairedService;
import com.demo.springboot.service.SortAscendingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api")
public class SortAscendingApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SortAscendingApiController.class);

    @Autowired
    private SortAscendingService sortAscendingService;

    @CrossOrigin
    @GetMapping(value = "/math/sortt/{param}")
    public ResponseEntity<SortAscendingService> testUnpaired(@PathVariable("param") String nmbrs)  {
        LOGGER.info("### Metoda testUnpaired");

        sortAscendingService.mathSortAsc(nmbrs);

        return new ResponseEntity<>(sortAscendingService, HttpStatus.OK);
    }
}
