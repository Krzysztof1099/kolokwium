package com.demo.springboot.rest;
import com.demo.springboot.service.SignToDeleteService;
import com.demo.springboot.service.WordToDeleteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/api")

public class WordToDeleteApiController  {
    private static final Logger LOGGER = LoggerFactory.getLogger(WordToDeleteApiController.class);

    @Autowired
    private WordToDeleteService wordToDeleteService;

    @CrossOrigin
    @GetMapping(value = "/math/text/removeWordd")
    public ResponseEntity<WordToDeleteService> testWord(@RequestParam(value = "text", required = false) String text,
                                                            @RequestParam(value = "word", required = false) String word)  {
        LOGGER.info("### Metoda testWord ### text: " + text +", word: " + word);

        wordToDeleteService.removeWord(text, word);

        return new ResponseEntity<>(wordToDeleteService, HttpStatus.OK);
    }
}
