package com.demo.springboot.rest;
import com.demo.springboot.service.AverageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/api")
public class AverageApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AverageApiController.class);

    @Autowired
    private AverageService averageService;

    //crossOrigin zapobiega wytwarzaniu lub wykorzystywaniu przez kod JS żądań o innym pochodzeniu
    @CrossOrigin // ogranicza zasoby zaimplementowane w przegladarkach internetowych
    @GetMapping(value = "/math/digitsss/{param}")
    public ResponseEntity<AverageService> testAverage(@PathVariable("param") String numbers){
        LOGGER.info("### testAverage is working!   === " + numbers);

        averageService.averageOf(numbers);

        return new ResponseEntity<>(averageService,HttpStatus.OK);
    }
}
