package com.demo.springboot.rest;

import com.demo.springboot.service.SearchUnpairedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api")
public class SearchUnpairedApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchUnpairedApiController.class);

    @Autowired
    private SearchUnpairedService searchUnpairedService;

    @CrossOrigin
    @GetMapping(value = "/math/getUnpaired/{param}")
    public ResponseEntity<SearchUnpairedService> testUnpaired(@PathVariable("param") String nmbrs)  {
        LOGGER.info("### Metoda testUnpaired");

        searchUnpairedService.numbersToSearch(nmbrs);

        return new ResponseEntity<>(searchUnpairedService, HttpStatus.OK);
    }
}
