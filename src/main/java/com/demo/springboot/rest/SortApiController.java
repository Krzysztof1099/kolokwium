package com.demo.springboot.rest;
import com.demo.springboot.service.SortingClassService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api")
public class SortApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SortApiController.class);

    @Autowired
    private SortingClassService sortingClassService;

    @CrossOrigin
    @GetMapping(value = "/math/sort/{params}")
    public ResponseEntity<SortingClassService> testSort(@PathVariable("params") String nmbrs){
        LOGGER.info("### Metoda testSort");

//        final SortingClass sortingClass = new SortingClass();
//        final String sortedNumbers = sortingClassService.numbersToSort(nmbrs);
        sortingClassService.numbersToSort(nmbrs);

        return new ResponseEntity<>(sortingClassService, HttpStatus.OK);
    }
}
