package com.demo.springboot.rest;
import com.demo.springboot.service.SignToDeleteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/api")
public class SignToDeleteApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SignToDeleteApiController.class);

    @Autowired
    private SignToDeleteService signToDeleteService;

    @CrossOrigin
    @GetMapping(value = "/math/text/removeCharacter")
    public ResponseEntity<SignToDeleteService> testUnpaired(@RequestParam(value = "text", required = false) String text,
                                                            @RequestParam(value = "character", required = false) String character)  {
        LOGGER.info("### Metoda testUnpaired");

        signToDeleteService.removeCharacter(text, character);

        return new ResponseEntity<>(signToDeleteService, HttpStatus.OK);
    }
}
