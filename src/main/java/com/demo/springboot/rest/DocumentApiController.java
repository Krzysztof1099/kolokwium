package com.demo.springboot.rest;


import com.demo.springboot.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/api")
public class DocumentApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);

    @Autowired
    private DocumentService documentService; // DocumentService documentService = new DocumentServiceImpl();

    @RequestMapping(value = "/document/test", method = RequestMethod.GET)
    public ResponseEntity<String> testDocument(){
        LOGGER.info("### Działa metoda testDocument");
        LOGGER.info("### Sprawdzam czy dziala wstrzykiwanie zaleznosci???");

        final String infoAboutDocument = documentService.getInfoAboutDocument();

//        final DocumentDto documentDto = new DocumentDto("Document", "pdf", 30.2);
//        LOGGER.info("Usluga zwroci nastepujace dane:");
//        LOGGER.info("### Nazwa dokumentu: {}", documentDto.getName());
//        LOGGER.info("### Rozmiar: {}", documentDto.getSize());
//        LOGGER.info("### Rozszerzenie: {}", documentDto.getExtension());
        LOGGER.info("### Komponent odpowiada: {}", infoAboutDocument);

        return new ResponseEntity<>(infoAboutDocument,HttpStatus.OK);
    }
}
