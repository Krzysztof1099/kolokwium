package com.demo.springboot.service;

public interface WordToDeleteService {
    String removeWord(String givenText, String givenWord);
}
