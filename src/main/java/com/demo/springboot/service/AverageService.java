package com.demo.springboot.service;

public interface AverageService {
    void averageOf(String numbers);
}
