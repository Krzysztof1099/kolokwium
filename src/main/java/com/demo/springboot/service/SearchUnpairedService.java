package com.demo.springboot.service;

public interface SearchUnpairedService {
    String numbersToSearch(String nmbrs) ;
}
