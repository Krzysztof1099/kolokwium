package com.demo.springboot.service;

public interface SignToDeleteService {
    String removeCharacter(String givenText, String givenCharacter);
}
