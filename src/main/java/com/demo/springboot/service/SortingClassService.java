package com.demo.springboot.service;

public interface SortingClassService {
    String numbersToSort(String numbers);
}
