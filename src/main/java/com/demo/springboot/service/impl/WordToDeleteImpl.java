package com.demo.springboot.service.impl;

import com.demo.springboot.service.WordToDeleteService;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

@Service
public class WordToDeleteImpl implements WordToDeleteService {
    String result;

    @Override
    public String removeWord(String givenText, String givenWord) {
        if (givenText.contains(givenWord)){
            //jesli slowo jest na poczatku lub w srodku
            String tmpWord = givenWord + " ";
            result = givenText.replaceAll(tmpWord,"");

            // jesli slowo jest na koncu stringa:
            tmpWord = " " + givenWord;
            result = givenText.replaceAll(tmpWord,"");
        }else {
            result = givenText;
        }
        return result;
    }
}
