package com.demo.springboot.service.impl;

import com.demo.springboot.service.SearchUnpairedService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

@Service
public class SearchUnpairedImpl implements SearchUnpairedService {
    //!!!!!!!!!!Zmienne w klasie z @Service sa zmiennymi ktore zostana zwrocone JSON'ie !!!!!!!!!
//    ArrayList<Integer> nmbrsList;
    String unpaired = new String("");

    @Override
    public String numbersToSearch(String nmbrs)  {
        unpaired = "";
        ArrayList<Integer> nmbrsList = new ArrayList<>();

        for (String splittedNumber : nmbrs.split(",")) {
            nmbrsList.add(Integer.parseInt(splittedNumber));
        }
//
//        //czysci powtorzone cyfry
        Set<Integer> integerSet = new HashSet<>(nmbrsList);
        nmbrsList.clear();
        nmbrsList.addAll(integerSet);

        for (int i = 0; i < nmbrsList.size(); i++) {
            if (nmbrsList.get(i)%2 != 0) {
                unpaired += nmbrsList.get(i) + " ";
            }
        }

        return unpaired;
    }
}
