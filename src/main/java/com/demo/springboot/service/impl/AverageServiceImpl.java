package com.demo.springboot.service.impl;
import com.demo.springboot.service.AverageService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

@Service
public class AverageServiceImpl implements AverageService {
    Double average;

    @Override
    public void averageOf(String numbers) {
        average = 0.0;

        if (numbers.isEmpty()) {
            average = 0.0;
        } else {
            Double[] nmbrsDouble = new Double[numbers.split(",").length];

            for (int i = 0; i < numbers.split(",").length; i++) {
                nmbrsDouble[i] = Double.parseDouble(numbers.split(",")[i]);
            }

            for (Double aDouble : nmbrsDouble) {
                average += aDouble;
            }
            average /= nmbrsDouble.length;
        }
//        return average.toString();
    }
}
