package com.demo.springboot.service.impl;

import com.demo.springboot.service.SortingClassService;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Getter                                             /* automatyczne generowanie getterow dla pol */
@ToString                                           /* automatyczne generowanie metody toString */
@NoArgsConstructor                                  /* automatyczne generowanie konstruktora bezparametrowego */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)         /* automatyczne generowanie zasiegu dla pol */

@Service
public class SortingClassImpl implements SortingClassService {
    Integer[] sortedNumbers;

    @Override
    public String numbersToSort(String numbers){
        String[] splittedNumbers = numbers.split(",");
        sortedNumbers = new Integer[splittedNumbers.length];
        for (int i = 0; i < splittedNumbers.length; i++) {
            sortedNumbers[i] = Integer.parseInt(splittedNumbers[i]);
        }
        Arrays.sort(sortedNumbers);
        return Arrays.toString(sortedNumbers);
    }
}
