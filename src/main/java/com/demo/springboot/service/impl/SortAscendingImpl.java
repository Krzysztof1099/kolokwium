package com.demo.springboot.service.impl;

import com.demo.springboot.service.SortAscendingService;
import javafx.beans.DefaultProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)

@Service
public class SortAscendingImpl implements SortAscendingService {
    String sort = new String("");

    @Override
    public String mathSortAsc(String nmbrs) {
        sort = "";
        Integer[] numbersInt = new Integer[nmbrs.split(",").length];

        for (int i = 0; i < nmbrs.split(",").length; i++) {
            numbersInt[i] = Integer.parseInt(nmbrs.split(",")[i]);
        }

        Arrays.sort(numbersInt);
        sort = Arrays.toString(numbersInt);
        return sort;
    }
}
