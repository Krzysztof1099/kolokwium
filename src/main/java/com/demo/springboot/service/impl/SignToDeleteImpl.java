package com.demo.springboot.service.impl;

import com.demo.springboot.service.SignToDeleteService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

@Service
public class SignToDeleteImpl implements SignToDeleteService {
    String result = new String("");

    @Override
    public String removeCharacter(String givenText, String givenCharacter) {
        result = "";
        result = givenText.replaceAll(givenCharacter,"");

        return result;
    }
}
