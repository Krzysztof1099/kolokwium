package com.demo.springboot.domain.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

/* lombok: */
@Getter                                             /* automatyczne generowanie getterow dla pol */
@ToString                                           /* automatyczne generowanie metody toString */
@NoArgsConstructor                                  /* automatyczne generowanie konstruktora bezparametrowego */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)         /* automatyczne generowanie zasiegu dla pol */

public class DocumentDto {
    String message;
}
